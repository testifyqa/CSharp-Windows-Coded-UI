﻿using System;
using System.CodeDom;
using BoDi;
using TechTalk.SpecFlow.Configuration;
using TechTalk.SpecFlow.Generator;
using TechTalk.SpecFlow.Generator.Configuration;
using TechTalk.SpecFlow.Generator.Plugins;
using TechTalk.SpecFlow.Generator.UnitTestProvider;
using TechTalk.SpecFlow.Utils;

namespace TechTalk.SpecFlow.CodedUI.MsTest
{
    public class CodedUiProviderPlugin : IGeneratorPlugin
    {
        public void Initialize(GeneratorPluginEvents generatorPluginEvents, GeneratorPluginParameters generatorPluginParameters)
        {
            generatorPluginEvents.CustomizeDependencies += GeneratorPluginEvents_CustomizeDependencies;
        }

        private void GeneratorPluginEvents_CustomizeDependencies(object sender, CustomizeDependenciesEventArgs eventArgs)
        {
            eventArgs.ObjectContainer.RegisterTypeAs<CodedUiGeneratorProvider, IUnitTestGeneratorProvider>();
        }

        public void RegisterCustomizations(ObjectContainer container, SpecFlowProjectConfiguration generatorConfiguration)
        {
            string unitTestProviderName = generatorConfiguration.SpecFlowConfiguration.UnitTestProvider;

            if (unitTestProviderName.Equals("mstest", StringComparison.InvariantCultureIgnoreCase) ||
                unitTestProviderName.Equals("mstest.2010", StringComparison.InvariantCultureIgnoreCase))
            {
                container.RegisterTypeAs<CodedUiGeneratorProvider, IUnitTestGeneratorProvider>();              
            }
        }

        public void RegisterDependencies(ObjectContainer container) { }

        public void RegisterConfigurationDefaults(SpecFlowConfiguration specFlowConfiguration) { }
    }

    public class CodedUiGeneratorProvider : MsTest2010GeneratorProvider
    {
        public CodedUiGeneratorProvider(CodeDomHelper codeDomHelper) : base(codeDomHelper) { }

        public override void SetTestClass(TestClassGenerationContext generationContext, string featureTitle, string featureDescription)
        {
            base.SetTestClass(generationContext, featureTitle, featureDescription);

            foreach (CodeAttributeDeclaration declaration in generationContext.TestClass.CustomAttributes)
            {
                if (declaration.Name == "Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute")
                {
                    generationContext.TestClass.CustomAttributes.Remove(declaration);
                    break;
                }
            }

            generationContext.TestClass.CustomAttributes.Add(new CodeAttributeDeclaration(
                new CodeTypeReference("Microsoft.VisualStudio.TestTools.UITesting.CodedUITestAttribute")));
        }
    }
}
