﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using TechTalk.SpecFlow;

namespace ProductAutomation.Hooks
{
    [Binding]
    public sealed class FeatureHooks
    {
        public static ApplicationUnderTest App;

        //Before & After hooks and handlers
        [BeforeFeature]
        public static void LaunchApplication()
        {
            if (FeatureContext.Current.FeatureInfo.Tags.Contains("App"))
            {           
                App = ApplicationUnderTest.Launch(Environment.CurrentDirectory + "\\..\\..\\..\\MyCalculatorv1\\bin\\Debug\\MyCalculatorv1.exe");
            }
        }

        [AfterFeature]
        public static void QuitApplication()
        {
            if (FeatureContext.Current.FeatureInfo.Tags.Contains("App"))
            {
                App.Close();
            }
        }
    }
}
