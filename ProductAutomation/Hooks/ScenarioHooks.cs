﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using TechTalk.SpecFlow;

namespace ProductAutomation.Hooks
{
    [Binding]
    public sealed class ScenarioHooks
    {
        public static ApplicationUnderTest App;

        //Before & After hooks and handlers
        [BeforeScenario]
        public static void LaunchApplication()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("App"))
            {
                App = ApplicationUnderTest.Launch(Environment.CurrentDirectory + "\\..\\..\\..\\MyCalculatorv1\\bin\\Debug\\MyCalculatorv1.exe"); 
            }
        }

        [AfterScenario]
        public static void QuitApplication()
        {
            if (ScenarioContext.Current.ScenarioInfo.Tags.Contains("App"))
            {
                App.Close();
            }
        }
    }
}
