﻿using Microsoft.VisualStudio.TestTools.UITesting;
using TechTalk.SpecFlow;

namespace ProductAutomation.Hooks
{
    [Binding]
    public sealed class TestRunHooks
    {
        //Before & After hooks and handlers
        [BeforeTestRun]
        public static void InitializePlayback()
        {
            Playback.Initialize();
        }

        [AfterTestRun]
        public static void CleanupPlayback()
        {
            Playback.Cleanup();
        }
    }
}
