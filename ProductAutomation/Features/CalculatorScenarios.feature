﻿@App
Feature: CalculatorScenarios
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Scenario: Add two numbers
	Given the calculator app has been cleared
	When I add two numbers together
	Then the displayed result is correct
