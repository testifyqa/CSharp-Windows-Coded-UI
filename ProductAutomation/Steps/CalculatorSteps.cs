﻿using ProductAutomation.UI.UIMapLoader;
using TechTalk.SpecFlow;

namespace ProductAutomation.Steps
{
    [Binding]
    public sealed class CalculatorSteps
    {
        [Given(@"the calculator app has been cleared")]
        public void GivenTheCalculatorAppHasBeenCleared()
        {
            CalculatorApp.Base.ClickDelButton();
        }

        [When(@"I add two numbers together")]
        public void WhenIAddTwoNumbersTogether()
        {
            CalculatorApp.Base.AddTwoNumbers(35, 15);
        }

        [Then(@"the displayed result is correct")]
        public void ThenTheDisplayedResultShouldIsCorrect()
        {
            CalculatorApp.Base.AssertResult(50);
        }
    }
}
