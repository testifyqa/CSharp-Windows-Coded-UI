﻿namespace ProductAutomation.UI.UIMapLoader
{
    public static class CalculatorApp
    {
        public static BaseUiMap Base
        {
            get { return _base ?? (_base = new BaseUiMap()); }
        }

        private static BaseUiMap _base;
    }
}
