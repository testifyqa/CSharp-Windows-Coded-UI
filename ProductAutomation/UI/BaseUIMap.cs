﻿using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProductAutomation.UI
{
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;


    public partial class BaseUiMap
    {        
        /// <summary>
        /// ClickDelButton
        /// </summary>
        public void ClickDelButton()
        {
            #region Variable Declarations
            WpfButton uIDelButton = this.UIMyCalculatorv1Window.UIDelButton;
            #endregion

            // Click 'Del' button
            Mouse.Click(uIDelButton, new Point(52, 24));
        }

        public void AddTwoNumbers(int numOne, int numTwo)
        {
            EnterNumber(numOne);
            ClickAddButton();
            EnterNumber(numTwo);
            ClickEqualsButton();
        }

        private void EnterNumber(int numberToEnter)
        {
            var calcTextField = UIMyCalculatorv1Window.UITBEdit;
            Keyboard.SendKeys(calcTextField, "^a" + "{RIGHT}" + numberToEnter.ToString());
        }

        /// <summary>
        /// ClickAddButton
        /// </summary>
        private void ClickAddButton()
        {
            #region Variable Declarations
            WpfButton uIItemButton = this.UIMyCalculatorv1Window.UIItemButton;
            #endregion

            // Click '+' button
            Mouse.Click(uIItemButton, new Point(44, 34));
        }

        /// <summary>
        /// ClickEqualsButton
        /// </summary>
        private void ClickEqualsButton()
        {
            #region Variable Declarations
            WpfButton uIItemButton1 = this.UIMyCalculatorv1Window.UIItemButton1;
            #endregion

            // Click '=' button
            Mouse.Click(uIItemButton1, new Point(78, 40));
        }

        /// <summary>
        /// AssertResult - Use 'AssertResultExpectedValues' to pass parameters into this method.
        /// </summary>
        public void AssertResult(int result)
        {
            #region Variable Declarations
            WpfEdit uITBEdit = this.UIMyCalculatorv1Window.UITBEdit;
            #endregion

            // Verify that the 'Text' property of 'tb' text box contains '50'
            StringAssert.Contains(uITBEdit.Text, result.ToString());
        }
    }
}